# coding: utf8
import pyaudio
import os
from yiwa.config import get_value_by_path
from yiwa.settings import BASE_DIR

APP_ID = "19390075"
API_KEY = "EQWranliB46VnGrcDjos1Hov"
SECRET_KEY = "l6G4Z6mt6rh9YrZG5VMR2WcON5T0qjcG"

AUDIO_OUTPUT = os.path.join(BASE_DIR, "asr", "output.wav")
AUDIO_FORMAT = "wav"
KEYWORD_FILE = os.path.join(BASE_DIR, "asr", "keywords.txt")

CUID = "18:65:90:cb:f0:51"  # 用户唯一标识，用来区分用户
# 百度AI有升级https://ai.baidu.com/forum/topic/show/958174
DEV_PID = 1537  # 普通话(支持简单的英文识别)

CHUNK = 1024
FORMAT = pyaudio.paInt16  # 16bit编码格式
CHANNELS = 1  # 单声道
RATE = 16000  # 16000采样频率

# 单次录音持续时间，单位：秒
TIME = get_value_by_path("yiwa", "listening", "recording_time", default=2)

# snowboy模型文件
MODEL_FILE = os.path.join(BASE_DIR, "snowboy", "yiwa.pmdl")
