# coding: utf8

"""yiwa-WEB后端"""

from __future__ import absolute_import
import subprocess
from flask import redirect, render_template
from tdcode import qr_code
from apps import app, socketio
from yiwa.db import DataConveyor
from yiwa.log import Log
from yiwa.settings import HOST, PORT, BASE_DIR
import os
from apps.__public._db import refresh_command


@app.route('/')
def hello_world():
    return redirect("/home")


@app.route("/refresh")
def refresh():
    """更新指令"""
    refresh_command()
    try:
        return render_template("yiwa/refresh.html")
    except:
        return None


@app.route("/reboot")
def reboot():
    """重启web"""

    def _exec(script):
        """执行shell脚本"""
        subprocess.call(script,
                        shell=True,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE)

    dc = DataConveyor()
    try:
        # 参考1，https://blog.csdn.net/weixin_42840933/article/details/85780125
        # 参考2，https://www.jianshu.com/p/bdfddc6ed505
        shell_path = os.path.join(BASE_DIR, "restart.sh")
        log_path = os.path.join(BASE_DIR, "logs", "nohup.log")
        _exec(f"nohup sh {shell_path} > {log_path} 2>&1 &")
        app.logger.info("重启成功")
    except Exception as error:
        app.logger.error(f"重启失败，报错：{error}")
        # 以下内容可能不会被display
        dc.info("重启失败")
    dc.info("重启成功")

    return redirect("/home")


if __name__ == '__main__':
    refresh()
    qr_code.clean_all()
    app.logger.addHandler(Log().handler)
    print(f"{HOST}:{PORT}")
    socketio.run(app, host=HOST, port=PORT, debug=True)
